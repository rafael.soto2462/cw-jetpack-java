/*
  Copyright (c) 2018 CommonsWare, LLC

  Licensed under the Apache License, Version 2.0 (the "License"); you may not
  use this file except in compliance with the License. You may obtain	a copy
  of the License at http://www.apache.org/licenses/LICENSE-2.0. Unless required
  by applicable law or agreed to in writing, software distributed under the
  License is distributed on an "AS IS" BASIS,	WITHOUT	WARRANTIES OR CONDITIONS
  OF ANY KIND, either express or implied. See the License for the specific
  language governing permissions and limitations under the License.

  Covered in detail in the book _Elements of Android Jetpack_

  https://commonsware.com/Jetpack
*/

package com.commonsware.jetpack.samplerj.actionbar;

import android.widget.Toast;
import com.commonsware.jetpack.samplerj.actionbar.databinding.RowBinding;
import androidx.recyclerview.widget.RecyclerView;

class ColorViewHolder extends RecyclerView.ViewHolder {
  private final RowBinding row;

  ColorViewHolder(RowBinding row) {
    super(row.getRoot());

    this.row = row;
    row.getRoot().setOnClickListener(
      v -> Toast.makeText(row.label.getContext(), row.label.getText(),
        Toast.LENGTH_LONG).show());
  }

  void bindTo(Integer color) {
    row.label.setText(
      row.label.getContext().getString(R.string.label_template, color));
    row.swatch.setBackgroundColor(color);
  }
}
