/*
  Copyright (c) 2018 CommonsWare, LLC

  Licensed under the Apache License, Version 2.0 (the "License"); you may not
  use this file except in compliance with the License. You may obtain	a copy
  of the License at http://www.apache.org/licenses/LICENSE-2.0. Unless required
  by applicable law or agreed to in writing, software distributed under the
  License is distributed on an "AS IS" BASIS,	WITHOUT	WARRANTIES OR CONDITIONS
  OF ANY KIND, either express or implied. See the License for the specific
  language governing permissions and limitations under the License.

  Covered in detail in the book _Elements of Android Jetpack_

  https://commonsware.com/Jetpack
*/

package com.commonsware.jetpack.samplerj.style;

import android.text.format.DateUtils;
import com.commonsware.jetpack.samplerj.style.databinding.RowBinding;
import androidx.recyclerview.widget.RecyclerView;

class EventViewHolder extends RecyclerView.ViewHolder {
  private final RowBinding row;
  private final long startTime;

  EventViewHolder(RowBinding row, long startTime) {
    super(row.getRoot());

    this.row = row;
    this.startTime = startTime;
  }

  void bindTo(Event event) {
    long elapsedSeconds = (event.timestamp - startTime)/1000;

    row.timestamp.setText(DateUtils.formatElapsedTime(elapsedSeconds));
    row.message.setText(event.message);
    row.activityHash.setText(Integer.toHexString(event.activityHash));
    row.viewmodelHash.setText(Integer.toHexString(event.viewmodelHash));
  }
}
