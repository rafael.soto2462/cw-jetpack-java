/*
  Copyright (c) 2019 CommonsWare, LLC

  Licensed under the Apache License, Version 2.0 (the "License"); you may not
  use this file except in compliance with the License. You may obtain	a copy
  of the License at http://www.apache.org/licenses/LICENSE-2.0. Unless required
  by applicable law or agreed to in writing, software distributed under the
  License is distributed on an "AS IS" BASIS,	WITHOUT	WARRANTIES OR CONDITIONS
  OF ANY KIND, either express or implied. See the License for the specific
  language governing permissions and limitations under the License.

  Covered in detail in the book _Elements of Android Jetpack_

  https://commonsware.com/Jetpack
*/

package com.commonsware.jetpack.contenteditor;

import android.Manifest;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;
import com.commonsware.jetpack.contenteditor.databinding.ActivityMainBinding;
import java.io.File;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;

public class MainActivity extends AppCompatActivity {
  private static final String FILENAME = "test.txt";
  private MainMotor motor;
  private Uri current;
  private ActivityMainBinding binding;

  private final ActivityResultLauncher<String[]> openDoc =
    registerForActivityResult(new ActivityResultContracts.OpenDocument(),
      new ActivityResultCallback<Uri>() {
        @Override
        public void onActivityResult(Uri uri) {
          binding.text.setText("");
          motor.read(uri);
        }
      });

  private final ActivityResultLauncher<String> createDoc =
    registerForActivityResult(new ActivityResultContracts.CreateDocument(),
      new ActivityResultCallback<Uri>() {
        @Override
        public void onActivityResult(Uri uri) {
          binding.text.setText("");
          motor.read(uri);
        }
      });

  private final ActivityResultLauncher<String> requestPerm =
    registerForActivityResult(new ActivityResultContracts.RequestPermission(),
      new ActivityResultCallback<Boolean>() {
        @Override
        public void onActivityResult(Boolean wasGranted) {
          if (wasGranted) {
            loadFromDir(Environment.getExternalStorageDirectory());
          }
          else {
            Toast.makeText(MainActivity.this, R.string.msg_sorry, Toast.LENGTH_LONG).show();
          }
        }
      });

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    binding = ActivityMainBinding.inflate(getLayoutInflater());
    setContentView(binding.getRoot());

    motor = new ViewModelProvider(this).get(MainMotor.class);

    motor.getResults().observe(this, result -> {
      current = result.source;

      binding.progress.setVisibility(result.isLoading ? View.VISIBLE : View.GONE);
      binding.text.setEnabled(!result.isLoading);

      if (result.source == null) {
        binding.title.setText("");
      }
      else {
        binding.title.setText(result.source.toString());
      }

      if (TextUtils.isEmpty(binding.text.getText())) {
        binding.text.setText(result.text);
      }

      if (result.error != null) {
        binding.text.setText(result.error.getLocalizedMessage());
        binding.text.setEnabled(false);
        Log.e("ContentEditor", "Exception in I/O", result.error);
      }
    });

    loadFromDir(getFilesDir());
  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    getMenuInflater().inflate(R.menu.actions, menu);

    return super.onCreateOptionsMenu(menu);
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    switch (item.getItemId()) {
      case R.id.loadInternal:
        loadFromDir(getFilesDir());
        return true;

      case R.id.loadExternal:
        loadFromDir(getExternalFilesDir(null));
        return true;

      case R.id.loadExternalRoot:
        requestPerm.launch(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        return true;

      case R.id.openDoc:
        openDoc.launch(new String[] { "text/*" });
        return true;

      case R.id.newDoc:
        createDoc.launch("text/plain");
        return true;

      case R.id.save:
        motor.write(current, binding.text.getText().toString());
        return true;
    }

    return super.onOptionsItemSelected(item);
  }

  private void loadFromDir(File dir) {
    binding.text.setText("");
    motor.read(Uri.fromFile(new File(dir, FILENAME)));
  }
}
