/*
  Copyright (c) 2019-2020 CommonsWare, LLC

  Licensed under the Apache License, Version 2.0 (the "License"); you may not
  use this file except in compliance with the License. You may obtain	a copy
  of the License at http://www.apache.org/licenses/LICENSE-2.0. Unless required
  by applicable law or agreed to in writing, software distributed under the
  License is distributed on an "AS IS" BASIS,	WITHOUT	WARRANTIES OR CONDITIONS
  OF ANY KIND, either express or implied. See the License for the specific
  language governing permissions and limitations under the License.

  Covered in detail in the book _Elements of Android Jetpack_

  https://commonsware.com/Jetpack
*/

package com.commonsware.jetpack.samplerj.nav;

import java.time.Instant;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.DiffUtil;

public class ToDoModel {
  @NonNull
  public final String id;
  @NonNull
  public final String description;
  public final boolean isCompleted;
  @Nullable
  public final String notes;
  @NonNull
  public final Instant createdOn;

  ToDoModel(@NonNull String id, @NonNull String description,
            boolean isCompleted, @Nullable String notes,
            @NonNull Instant createdOn) {
    this.id = id;
    this.description = description;
    this.isCompleted = isCompleted;
    this.notes = notes;
    this.createdOn = createdOn;
  }

  static final DiffUtil.ItemCallback<ToDoModel> DIFF_CALLBACK =
    new DiffUtil.ItemCallback<ToDoModel>() {
      @Override
      public boolean areItemsTheSame(@NonNull ToDoModel oldItem,
                                     @NonNull ToDoModel newItem) {
        return oldItem == newItem;
      }

      @Override
      public boolean areContentsTheSame(@NonNull ToDoModel oldItem,
                                        @NonNull ToDoModel newItem) {
        return oldItem.isCompleted == newItem.isCompleted &&
          oldItem.description.equals(newItem.description);
      }
    };
}
