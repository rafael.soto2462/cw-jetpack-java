/*
  Copyright (c) 2019 CommonsWare, LLC

  Licensed under the Apache License, Version 2.0 (the "License"); you may not
  use this file except in compliance with the License. You may obtain	a copy
  of the License at http://www.apache.org/licenses/LICENSE-2.0. Unless required
  by applicable law or agreed to in writing, software distributed under the
  License is distributed on an "AS IS" BASIS,	WITHOUT	WARRANTIES OR CONDITIONS
  OF ANY KIND, either express or implied. See the License for the specific
  language governing permissions and limitations under the License.

  Covered in detail in the book _Elements of Android Jetpack_

  https://commonsware.com/Jetpack
*/

package com.commonsware.jetpack.weather;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.moshi.MoshiConverterFactory;

class WeatherRepository {
  private static volatile WeatherRepository INSTANCE;
  private final NWSInterface api;

  synchronized static WeatherRepository get() {
    if (INSTANCE == null) {
      INSTANCE = new WeatherRepository();
    }

    return INSTANCE;
  }

  private WeatherRepository() {
    Retrofit retrofit=
      new Retrofit.Builder()
        .baseUrl("https://api.weather.gov")
        .addConverterFactory(MoshiConverterFactory.create())
        .build();

    api = retrofit.create(NWSInterface.class);
  }

  LiveData<WeatherResult> load(String office, int gridX, int gridY) {
    final MutableLiveData<WeatherResult> result = new MutableLiveData<>();

    result.setValue(new WeatherResult(true, null, null));

    api.getForecast(office, gridX, gridY).enqueue(
      new Callback<WeatherResponse>() {
        @Override
        public void onResponse(Call<WeatherResponse> call,
                               Response<WeatherResponse> response) {
          result.postValue(new WeatherResult(false, response.body().properties.periods, null));
        }

        @Override
        public void onFailure(Call<WeatherResponse> call, Throwable t) {
          result.postValue(new WeatherResult(false, null, t));
        }
      });

    return result;
  }
}
