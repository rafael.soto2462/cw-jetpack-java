/*
  Copyright (c) 2019-2021 CommonsWare, LLC

  Licensed under the Apache License, Version 2.0 (the "License"); you may not
  use this file except in compliance with the License. You may obtain	a copy
  of the License at http://www.apache.org/licenses/LICENSE-2.0. Unless required
  by applicable law or agreed to in writing, software distributed under the
  License is distributed on an "AS IS" BASIS,	WITHOUT	WARRANTIES OR CONDITIONS
  OF ANY KIND, either express or implied. See the License for the specific
  language governing permissions and limitations under the License.

  Covered in detail in the book _Elements of Android Jetpack_

  https://commonsware.com/Jetpack
*/

package com.commonsware.jetpack.diceware;

import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import com.commonsware.jetpack.diceware.databinding.ActivityMainBinding;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;

public class MainActivity extends AppCompatActivity {
  private MainMotor motor;

  private final ActivityResultLauncher<String[]> openDoc =
    registerForActivityResult(new ActivityResultContracts.OpenDocument(),
      new ActivityResultCallback<Uri>() {
        @Override
        public void onActivityResult(Uri uri) {
          motor.generatePassphrase(uri);
        }
      });

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    ActivityMainBinding binding =
      ActivityMainBinding.inflate(getLayoutInflater());

    setContentView(binding.getRoot());

    motor = new ViewModelProvider(this).get(MainMotor.class);

    motor.viewStates.observe(this, viewState -> {
      binding.progress.setVisibility(
        viewState.isLoading ? View.VISIBLE : View.GONE);

      if (viewState.content != null) {
        binding.passphrase.setText(viewState.content);
      }
      else if (viewState.error != null) {
        binding.passphrase.setText(viewState.error.getLocalizedMessage());
        Log.e("Diceware", "Exception generating passphrase",
          viewState.error);
      }
      else {
        binding.passphrase.setText("");
      }
    });
  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    getMenuInflater().inflate(R.menu.actions, menu);

    return super.onCreateOptionsMenu(menu);
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    switch (item.getItemId()) {
      case R.id.open:
        openDoc.launch(new String[]{"text/plain"});

        return true;

      case R.id.refresh:
        motor.generatePassphrase();
        return true;

      case R.id.word_count_4:
      case R.id.word_count_5:
      case R.id.word_count_6:
      case R.id.word_count_7:
      case R.id.word_count_8:
      case R.id.word_count_9:
      case R.id.word_count_10:
        item.setChecked(!item.isChecked());
        motor.generatePassphrase(Integer.parseInt(item.getTitle().toString()));

        return true;
    }

    return super.onOptionsItemSelected(item);
  }
}
