/*
  Copyright (c) 2019 CommonsWare, LLC

  Licensed under the Apache License, Version 2.0 (the "License"); you may not
  use this file except in compliance with the License. You may obtain	a copy
  of the License at http://www.apache.org/licenses/LICENSE-2.0. Unless required
  by applicable law or agreed to in writing, software distributed under the
  License is distributed on an "AS IS" BASIS,	WITHOUT	WARRANTIES OR CONDITIONS
  OF ANY KIND, either express or implied. See the License for the specific
  language governing permissions and limitations under the License.

  Covered in detail in the book _Elements of Android Jetpack_

  https://commonsware.com/Jetpack
*/

package com.commonsware.jetpack.samplerj.fragments;

import android.view.LayoutInflater;
import android.view.ViewGroup;
import com.commonsware.jetpack.samplerj.fragments.databinding.TodoRowBinding;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.ListAdapter;

class ToDoListAdapter extends ListAdapter<ToDoModel, ToDoListRowHolder> {
  private final LayoutInflater inflater;
  private final ToDoListRowHolder.OnRowClickListener listener;

  protected ToDoListAdapter(LayoutInflater inflater,
                            ToDoListRowHolder.OnRowClickListener listener) {
    super(ToDoModel.DIFF_CALLBACK);
    this.inflater = inflater;
    this.listener = listener;
  }

  @NonNull
  @Override
  public ToDoListRowHolder onCreateViewHolder(@NonNull ViewGroup parent,
                                              int viewType) {
    return new ToDoListRowHolder(
      TodoRowBinding.inflate(inflater, parent, false), listener);
  }

  @Override
  public void onBindViewHolder(@NonNull ToDoListRowHolder holder,
                               int position) {
    holder.bind(getItem(position));
  }
}
